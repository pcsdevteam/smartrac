package com.pcs.app.smarttrack;

import android.os.CountDownTimer;
import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class HomeActivity extends Activity {

    Spinner spinnerDropDown1,str_BusNo;
    CountDownTimer newtimer;
    TextView timeTxt,dateTxt;
    Button submitBtn,clearBtn;
    EditText str_SeatNo;
    TextView str_TravelID;
    String[] issuestype = {
            "TV  -  Sensor",
            "TV  -  Adapter Loose connection",
            "TV  -  Screen, Multi Color",
            "AndroidBox - Adapter Loose connection",
            "AndroidBox - Electric socket pin",
            "Remotes - Replace",
            "Remotes - Air mouse malfunctioning",
            "Remotes - Batteries",
            "Enclosure -  Bolts",
            "Enclosure - Screws",
            "Head phones -  Pin @ TV",
            "Head phones - Pin @ passnger side",
            "Software -App setting issue",
            "Software -App Config issue",
            "Infra - Outside-wires ( Power )",
            "Infra - Outside-wires (  LAN )",
            "Infra - Outside-wires (  RF cables )",
            "Electrical - Inverter Issue",
            "Content - Pen Drive issue",
            "Content - Hard drive Drive issue",
            "Other"
    };
    String[] busnumbers = {
            "AR-02-4203",
            "AR-02-4201",
            "AR-02-4199",
            "AR-02-4202",
            "AR-02-4204",
            "AR-02-4200",
            "AR-02-4198",
            "AR-02-4197",
            "AR-01-7818"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // Get reference of SpinnerView from layout/main_activity.xml
        spinnerDropDown1 =(Spinner)findViewById(R.id.listView);
        timeTxt = (TextView)findViewById(R.id.timeid);
        dateTxt = (TextView)findViewById(R.id.dateid);
        submitBtn = (Button)findViewById(R.id.submit);
        clearBtn = (Button)findViewById(R.id.clear);
        str_BusNo = (Spinner)findViewById(R.id.editBusno);
        str_SeatNo = (EditText)findViewById(R.id.editseatno);
        str_TravelID = (TextView)findViewById(R.id.travelid);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,issuestype);

        spinnerDropDown1.setAdapter(adapter);

        spinnerDropDown1.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // Get select item
                int sid=spinnerDropDown1.getSelectedItemPosition();
               // Toast.makeText(getBaseContext(), "You have selected Issue Type : " + issuestype[sid],
                       // Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<String> adapter1= new ArrayAdapter<String>(this,android.
                R.layout.simple_spinner_dropdown_item ,busnumbers);

        str_BusNo.setAdapter(adapter1);

        str_BusNo.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // Get select item
                int sid=spinnerDropDown1.getSelectedItemPosition();
                // Toast.makeText(getBaseContext(), "You have selected Issue Type : " + issuestype[sid],
                // Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        newtimer = new CountDownTimer(1000000000, 1000) {

            public void onTick(long millisUntilFinished) {
                Calendar c = Calendar.getInstance();
                timeTxt.setText("Time : "+c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND));
                java.text.SimpleDateFormat df1 = new java.text.SimpleDateFormat("MM");
                dateTxt.setText("Date : "+c.get(Calendar.DAY_OF_MONTH)+"/"+df1.format(c.getTime())+"/"+c.get(Calendar.YEAR));
            }
            public void onFinish() {

            }
        };
        newtimer.start();
        View.OnClickListener clearListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                str_SeatNo.setText("");
                spinnerDropDown1.setSelection(0);
            }
        };
        clearBtn.setOnClickListener(clearListener);
        View.OnClickListener submitListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = "OTT"+"/"+busnumbers[str_BusNo.getSelectedItemPosition()]+"/"+str_SeatNo.getText().toString()+
                        "/"+issuestype[spinnerDropDown1.getSelectedItemPosition()]+"("+dateTxt.getText()+"-"+timeTxt.getText()+")";
                //sendSMS("6092138073",str);
                //sendSMS("9259839273",str);
                sendSMS("9959325555",str);
                sendSMS("9989518210",str);
                sendSMS("9100963199",str);
                sendSMS("9951809992",str);
                hideKeyboard();
            }
        };
        submitBtn.setOnClickListener(submitListener);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    private void sendSMS(String phoneNumber, String message) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
        Toast.makeText(getBaseContext(), "Sent Msg  : " + message,
                 Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        newtimer.cancel();
    }
}
